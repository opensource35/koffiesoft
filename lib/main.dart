import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:koffiesoft/app/modules/auth/views/login_view.dart';
import 'package:koffiesoft/app/modules/splashscreen/view/splash_screen_view.dart';
import 'package:koffiesoft/app/routes/app_pages.dart';

void main() async {
  await GetStorage.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
   Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreenView(),
      getPages: AppPages.routes,
    );
  }
}
