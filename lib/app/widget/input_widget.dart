import 'package:flutter/material.dart';
import 'package:koffiesoft/app/modules/auth/controllers/auth_controller.dart';

class InputWidget{
  static Widget genderDropDown({required AuthController controller, required List<Map<String,dynamic>> items, Map<String,dynamic>? value}){
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("Gender"),
          const SizedBox(height: 10,),
          DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.black38),
            ),
            child: Padding(
              padding: const EdgeInsets.only(left:15, right:10),
              child:DropdownButton(
                  value: value,
                  items: items.map((Map<String,dynamic> value) {
                    return DropdownMenuItem<Map<String,dynamic>>(
                      value: value,
                      child: Text(value["text"]),
                    );
                  }).toList(),
                  onChanged: (value){
                    controller.changeDropDownGender(value);
                  },
                  isExpanded: true, //make true to take width of parent widget
                  underline: Container(),//empty line
                  style: const TextStyle(color: Colors.black),
                )
            )
          ),
        ],
      ),
    );
  }
}