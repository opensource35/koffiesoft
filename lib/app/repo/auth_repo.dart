import 'package:koffiesoft/config.dart';

class AuthRepo {
  static const login = api + "login";
  static const register = api + "users";
  static const otp = api + "users/otp";
  static const verification = api + "users/verifikasi";
}
