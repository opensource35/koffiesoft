import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:koffiesoft/app/routes/app_routes.dart';
import 'package:koffiesoft/app/utils/storage_utils.dart';

class HomeController extends GetxController{
  void logout(){
    GetStorage().remove(MyStorage.token);

    Get.offAllNamed(Routes.LOGIN);
  }
}