import 'package:get/get.dart';
import 'package:koffiesoft/app/modules/auth/controllers/auth_controller.dart';
import 'package:koffiesoft/app/modules/home/controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
