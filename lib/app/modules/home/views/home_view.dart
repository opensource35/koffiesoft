import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koffiesoft/app/modules/home/controllers/home_controller.dart';

class HomeView extends StatelessWidget {
  final HomeController controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Home"),actions: [
        IconButton(onPressed: (){
          Get.defaultDialog(
            contentPadding: const EdgeInsets.all(20),
            title: "",
            confirm: TextButton(onPressed: (){ controller.logout(); }, child: const Text("Ya, Keluar",style: TextStyle(color: Colors.red),)),
            middleText: "Yakin Ingin Keluar ?",
            cancel: TextButton(onPressed: () => Get.back(), child: const Text("Batal"))
          );
        }, icon: const Icon(Icons.logout),)
      ],),
      body: const Center(child: Text("Selamat Datang di Home")),
    );
  }
}