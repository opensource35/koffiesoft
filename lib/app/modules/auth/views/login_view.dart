import 'package:koffiesoft/app/modules/auth/controllers/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koffiesoft/app/routes/app_routes.dart';

class LoginView extends StatelessWidget {
  LoginView({Key? key}) : super(key: key);

  final AuthController controller = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 100,
            horizontal: 15,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 80,
                width: 80,
                child: CircleAvatar(
                  child: Icon(Icons.lock),
                ),
              ),
              inputField(
                label: "Email",
                hintText: "",
                textcontroller: controller.email,
              ),
              inputField(
                label: "Password",
                hintText: "",
                obscureText: true,
                textcontroller: controller.password,
              ),
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                width: Get.width,
                height: 45,
                child: ElevatedButton(
                  onPressed: () {
                    controller.doLogin();
                  },
                  child: const Text(
                    "Masuk",
                  ),
                  style: ElevatedButton.styleFrom(
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("Belum memiliki akun?"),
                  const SizedBox(width: 5,),
                  GestureDetector(onTap: (){
                    Get.offAndToNamed(Routes.REGISTER);
                  } ,child: const Text("daftar disini",style: TextStyle(color: Colors.blue),)),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Column inputField(
      {required String label,
      required String hintText,
      bool obscureText = false,
      TextEditingController? textcontroller,
      String typeInput = "text"}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 50,
          child: TextFormField(
            controller: textcontroller,
            obscureText: obscureText,
            keyboardType:
                typeInput == "text" ? TextInputType.text : TextInputType.number,
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                hintText: hintText,
                hintStyle: const TextStyle(
                  fontSize: 14,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                )),
          ),
        ),
      ],
    );
  }
}
