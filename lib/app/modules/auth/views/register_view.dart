import 'package:http/http.dart';
import 'package:koffiesoft/app/modules/auth/controllers/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koffiesoft/app/routes/app_routes.dart';
import 'package:koffiesoft/app/utils/data_user_utils.dart';
import 'package:koffiesoft/app/widget/input_widget.dart';

class RegisterView extends StatelessWidget {
  RegisterView({Key? key}) : super(key: key);

  final AuthController controller = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 100,
            horizontal: 15,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 80,
                width: 80,
                child: CircleAvatar(
                  child: Icon(Icons.lock),
                ),
              ),
              inputField(
                label: "Email",
                hintText: "",
                textcontroller: controller.emailReg,
              ),
              inputField(
                label: "Phone Number",
                hintText: "",
                typeInput: "number",
                textcontroller: controller.phoneNumber,
              ),
              inputField(
                label: "Nama Depan",
                hintText: "",
                textcontroller: controller.firtsName,
              ),
              inputField(
                label: "Nama Belakang",
                hintText: "",
                textcontroller: controller.lastName,
              ),
              inputField(
                onTap: (){
                  showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2099),
                  ).then((date){
                    print(date);
                    date != null ? controller.handlePickDate(date) : null;
                  });
                },
                readOnly: true,
                label: "Tanggal Lahir",
                hintText: "",
                textcontroller: controller.birthDayDate,
              ),
              GetBuilder<AuthController>(
                builder: (context) {
                  return InputWidget.genderDropDown(controller: controller,items: DataUser.gender, value: controller.valueGender);
                }
              ),
              inputField(
                label: "Password",
                hintText: "",
                obscureText: true,
                textcontroller: controller.passwordReg,
              ),
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                width: Get.width,
                height: 45,
                child: ElevatedButton(
                  onPressed: () {
                    controller.doRegister();
                  },
                  child: const Text(
                    "Daftar",
                  ),
                  style: ElevatedButton.styleFrom(
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("Sudah memiliki akun?"),
                  const SizedBox(width: 5,),
                  GestureDetector(onTap: (){
                    Get.offAndToNamed(Routes.LOGIN);
                  } ,child: const Text("Masuk disini",style: TextStyle(color: Colors.blue),)),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Container inputField(
      {required String label,
      required String hintText,
      Function? onTap,
      bool obscureText = false,
      bool readOnly = false,
      TextEditingController? textcontroller,
      String typeInput = "text"}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 50,
            child: TextFormField(
              onTap: (){
                onTap != null ? onTap() : null;
              },
              readOnly: readOnly,
              controller: textcontroller,
              obscureText: obscureText,
              keyboardType:
                  typeInput == "text" ? TextInputType.text : TextInputType.number,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                  hintText: hintText,
                  hintStyle: TextStyle(
                    fontSize: 14,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
