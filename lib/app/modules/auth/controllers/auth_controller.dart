import 'dart:convert';
import 'package:get_storage/get_storage.dart';
import 'package:koffiesoft/app/data/models/login_model.dart';
import 'package:koffiesoft/app/data/models/register_model.dart';
import 'package:koffiesoft/app/data/models/status_model.dart';
import 'package:koffiesoft/app/data/models/validation_model.dart';
import 'package:koffiesoft/app/data/models/verification_model.dart';
import 'package:koffiesoft/app/repo/auth_repo.dart';
import 'package:koffiesoft/app/routes/app_routes.dart';
import 'package:koffiesoft/app/utils/storage_utils.dart';
import 'package:koffiesoft/app/utils/tools.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:koffiesoft/config.dart';
class AuthController extends GetxController {
  bool loadingPage = false;
  String? credential;

  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  TextEditingController emailReg = TextEditingController();
  TextEditingController passwordReg = TextEditingController();
  TextEditingController phoneNumber = TextEditingController();
  TextEditingController firtsName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController birthDayDate = TextEditingController();
  DateTime? birthDayDateTime;
  Map<String,dynamic>? valueGender;

  // value otp
  TextEditingController otpfirst = TextEditingController();
  TextEditingController otpsecond = TextEditingController();
  TextEditingController otpthird = TextEditingController();
  TextEditingController otpfourth = TextEditingController();
  TextEditingController otpfifth = TextEditingController();
  TextEditingController otpsixth = TextEditingController();

  @override
  void onInit() {
    if(Get.arguments != null){
      credential = Get.arguments["email"];

      print(credential);
    }

    super.onInit();
  }

  void changeDropDownGender(value){
    valueGender = value;
    update();
  }

  void back() {
    emptyInput();
    Get.back();
  }

  void emptyInput() {
    otpfirst.text = "";
    otpsecond.text = "";
    otpthird.text = "";
    otpfourth.text = "";
  }

  void handlePickDate(DateTime value){
    String date = DateFormat.yMMMMd().format(value);
    birthDayDateTime = value;
    birthDayDate.text = date;
    update();
  }

  Future doLogin() async {
    dialogCircular();

    Map<String,dynamic> body = {
      "username": email.text,
      "password": password.text
    };

    http.Response res = await http.post(Uri.parse(AuthRepo.login),headers: headerUrlEncoded, body: body);

    if(res.statusCode == 200){
      print(res.body);
      LoginModel result = LoginModel.fromJson(json.decode(res.body));
      
      GetStorage().write(MyStorage.token, result.accessToken);

      Get.offAllNamed(Routes.HOME);
      alertSuccess(content: "Login Berhasil");
    }else if(res.statusCode == 422){
      ValidationModel validation = ValidationModel.fromJson(json.decode(res.body));
      Get.back();
      alertValidation(content: validation);
    }else if(res.statusCode == 404){
      Map<String,dynamic> result = json.decode(res.body);
      Get.back();
      alertError(content: [result["status"]["keterangan"]]);
    }
  }

  Future doRegister()async{
    dialogCircular();

    String? date = birthDayDateTime == null ? null :  DateFormat("yyyy-MM-dd").format(birthDayDateTime!);

    Map<String,dynamic> body = {
        "email": emailReg.text,
        "password": passwordReg.text,
        "hp": phoneNumber.text,
        "firstname": firtsName.text,
        "lastname": lastName.text,
        "tgl_lahir": date,
        "jenis_kelamin": valueGender?["code"],
        "grup": "member",
    };

    http.Response res = await http.post(Uri.parse(AuthRepo.register),headers: header,body: json.encode(body));

    if(res.statusCode == 200){
      RegisterModel result = RegisterModel.fromJson(jsonDecode(res.body));

      Get.back();
      Get.toNamed(Routes.VERIFICATION,arguments: {"email": emailReg.text});
      alertSuccess(content: result.status!.keterangan);
    }else if(res.statusCode == 210){
      Get.back();
      Get.toNamed(Routes.VERIFICATION);
      alertError(content: ["OTP Gagal Dikirim, Silah Kirim Ulang Kode OTP"]);
    }else if(res.statusCode == 422){
      ValidationModel validation = ValidationModel.fromJson(json.decode(res.body));
      Get.back();
      alertValidation(content: validation);
    }else if(res.statusCode == 400){
      Map<String,dynamic> result = json.decode(res.body);
      Get.back();
      alertError(content: [result["status"]["keterangan"]]);
    }else if(res.statusCode == 404){
      Map<String,dynamic> result = json.decode(res.body);
      Get.back();
      alertError(content: [result["status"]["keterangan"]]);
    }
  }

  Future doResendOtp()async{
    dialogCircular();

    Map<String,dynamic> body = {
      "credential" : credential,
      "tujuan": "email",
      "zona_waktu": "Asia/Jakarta"
    };

    http.Response res = await http.post(Uri.parse(AuthRepo.otp),headers: header, body: json.encode(body));

    if(res.statusCode == 200){
      StatusModel result = StatusModel.fromJson(json.decode(res.body));

      alertSuccess(content: "OTP berhasil dikirim");
    }else if(res.statusCode == 422){
      ValidationModel validation = ValidationModel.fromJson(json.decode(res.body));
      Get.back();
      alertValidation(content: validation);
    }else if(res.statusCode == 404 || res.statusCode == 400){
      Map<String,dynamic> result = json.decode(res.body);
      Get.back();
      alertError(content: [result["status"]["keterangan"]]);
    }
  }

  Future doVerification()async{
    dialogCircular();
    String otp = otpfirst.text + otpsecond.text + otpthird.text + otpfourth.text + otpfifth.text + otpsixth.text;

    Map<String,dynamic> body = {
      "credential" : credential,
      "otp": otp,
    };

    http.Response res = await http.post(Uri.parse(AuthRepo.verification),headers: header, body: json.encode(body));

    if(res.statusCode == 200){
      VerificationModel result = VerificationModel.fromJson(json.decode(res.body));
      
      Get.offAndToNamed(Routes.LOGIN);
      alertSuccess(content: "OTP berhasil diverifikasi, silahkan login");
    }else if(res.statusCode == 422){
      ValidationModel validation = ValidationModel.fromJson(json.decode(res.body));
      Get.back();
      alertValidation(content: validation);
    }else if(res.statusCode == 404 || res.statusCode == 400){
      Map<String,dynamic> result = json.decode(res.body);
      Get.back();
      alertError(content: [result["status"]["keterangan"]]);
    }
  }
}
