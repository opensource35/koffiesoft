import 'dart:async';

import 'package:koffiesoft/app/routes/app_routes.dart';
import 'package:get/get.dart';
import 'package:koffiesoft/app/utils/storage_utils.dart';

class SplashScreenController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() async {
    super.onReady();
    await Future.delayed(Duration(seconds: 5));

    if (MyStorage.getToken() == null) {
      Get.offAndToNamed(Routes.LOGIN);
    } else {
      Get.offAndToNamed(Routes.HOME);
    }
  }
}
