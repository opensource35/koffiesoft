class StatusModel {
  Status? status;

  StatusModel({this.status});

  StatusModel.fromJson(Map<String, dynamic> json) {
    status =
        json['status'] != null ? new Status.fromJson(json['status']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.status != null) {
      data['status'] = this.status!.toJson();
    }
    return data;
  }
}

class Status {
  String? kode;
  String? keterangan;

  Status({this.kode, this.keterangan});

  Status.fromJson(Map<String, dynamic> json) {
    kode = json['kode'];
    keterangan = json['keterangan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kode'] = this.kode;
    data['keterangan'] = this.keterangan;
    return data;
  }
}
