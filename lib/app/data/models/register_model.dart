class RegisterModel {
  Status? status;
  Data? data;

  RegisterModel({this.status, this.data});

  RegisterModel.fromJson(Map<String, dynamic> json) {
    status =
        json['status'] != null ? new Status.fromJson(json['status']) : null;
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.status != null) {
      data['status'] = this.status!.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Status {
  String? kode;
  String? keterangan;

  Status({this.kode, this.keterangan});

  Status.fromJson(Map<String, dynamic> json) {
    kode = json['kode'];
    keterangan = json['keterangan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kode'] = this.kode;
    data['keterangan'] = this.keterangan;
    return data;
  }
}

class Data {
  String? email;
  String? hp;
  String? firstname;
  String? lastname;
  String? grup;
  String? role;
  String? tglLahir;
  int? jenisKelamin;
  int? id;
  StatusData? status;
  Status? accountStatus;
  Photo? photo;
  Toko? toko;

  Data(
      {this.email,
      this.hp,
      this.firstname,
      this.lastname,
      this.grup,
      this.role,
      this.tglLahir,
      this.jenisKelamin,
      this.id,
      this.status,
      this.accountStatus,
      this.photo,
      this.toko});

  Data.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    hp = json['hp'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    grup = json['grup'];
    role = json['role'];
    tglLahir = json['tgl_lahir'];
    jenisKelamin = json['jenis_kelamin'];
    id = json['id'];
    status =
        json['status'] != null ? new StatusData.fromJson(json['status']) : null;
    accountStatus = json['account_status'] != null
        ? new Status.fromJson(json['account_status'])
        : null;
    photo = json['photo'] != null ? new Photo.fromJson(json['photo']) : null;
    toko = json['toko'] != null ? new Toko.fromJson(json['toko']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['hp'] = this.hp;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['grup'] = this.grup;
    data['role'] = this.role;
    data['tgl_lahir'] = this.tglLahir;
    data['jenis_kelamin'] = this.jenisKelamin;
    data['id'] = this.id;
    if (this.status != null) {
      data['status'] = this.status!.toJson();
    }
    if (this.accountStatus != null) {
      data['account_status'] = this.accountStatus!.toJson();
    }
    if (this.photo != null) {
      data['photo'] = this.photo!.toJson();
    }
    if (this.toko != null) {
      data['toko'] = this.toko!.toJson();
    }
    return data;
  }
}

class StatusData {
  int? kode;
  String? keterangan;

  StatusData({this.kode, this.keterangan});

  StatusData.fromJson(Map<String, dynamic> json) {
    kode = json['kode'];
    keterangan = json['keterangan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kode'] = this.kode;
    data['keterangan'] = this.keterangan;
    return data;
  }
}

class Photo {
  int? id;
  String? filename;
  String? caption;
  int? width;
  int? height;
  String? contentType;
  String? uri;

  Photo(
      {this.id,
      this.filename,
      this.caption,
      this.width,
      this.height,
      this.contentType,
      this.uri});

  Photo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    filename = json['filename'];
    caption = json['caption'];
    width = json['width'];
    height = json['height'];
    contentType = json['content_type'];
    uri = json['uri'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['filename'] = this.filename;
    data['caption'] = this.caption;
    data['width'] = this.width;
    data['height'] = this.height;
    data['content_type'] = this.contentType;
    data['uri'] = this.uri;
    return data;
  }
}

class Toko {
  String? nama;
  String? jalan;
  String? kelurahan;
  String? kecamatan;
  String? kota;
  String? provinsi;
  String? kodepos;
  String? detailAlamat;
  int? longitude;
  int? latitude;
  String? telp;
  String? email;
  String? slogan;
  String? deskripsi;
  String? aturanBelanja;
  String? aturanRetur;
  int? id;
  Status? status;
  List<Logo>? logo;

  Toko(
      {this.nama,
      this.jalan,
      this.kelurahan,
      this.kecamatan,
      this.kota,
      this.provinsi,
      this.kodepos,
      this.detailAlamat,
      this.longitude,
      this.latitude,
      this.telp,
      this.email,
      this.slogan,
      this.deskripsi,
      this.aturanBelanja,
      this.aturanRetur,
      this.id,
      this.status,
      this.logo});

  Toko.fromJson(Map<String, dynamic> json) {
    nama = json['nama'];
    jalan = json['jalan'];
    kelurahan = json['kelurahan'];
    kecamatan = json['kecamatan'];
    kota = json['kota'];
    provinsi = json['provinsi'];
    kodepos = json['kodepos'];
    detailAlamat = json['detail_alamat'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    telp = json['telp'];
    email = json['email'];
    slogan = json['slogan'];
    deskripsi = json['deskripsi'];
    aturanBelanja = json['aturan_belanja'];
    aturanRetur = json['aturan_retur'];
    id = json['id'];
    status =
        json['status'] != null ? new Status.fromJson(json['status']) : null;
    if (json['logo'] != null) {
      logo = <Logo>[];
      json['logo'].forEach((v) {
        logo!.add(new Logo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nama'] = this.nama;
    data['jalan'] = this.jalan;
    data['kelurahan'] = this.kelurahan;
    data['kecamatan'] = this.kecamatan;
    data['kota'] = this.kota;
    data['provinsi'] = this.provinsi;
    data['kodepos'] = this.kodepos;
    data['detail_alamat'] = this.detailAlamat;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['telp'] = this.telp;
    data['email'] = this.email;
    data['slogan'] = this.slogan;
    data['deskripsi'] = this.deskripsi;
    data['aturan_belanja'] = this.aturanBelanja;
    data['aturan_retur'] = this.aturanRetur;
    data['id'] = this.id;
    if (this.status != null) {
      data['status'] = this.status!.toJson();
    }
    if (this.logo != null) {
      data['logo'] = this.logo!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Logo {
  List<Logo>? logo;

  Logo({this.logo});

  Logo.fromJson(Map<String, dynamic> json) {
    if (json['logo'] != null) {
      logo = <Logo>[];
      json['logo'].forEach((v) {
        logo!.add(new Logo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.logo != null) {
      data['logo'] = this.logo!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

