class LoginModel {
  JenisKelamin? status;
  String? accessToken;
  String? tokenType;
  Data? data;

  LoginModel({this.status, this.accessToken, this.tokenType, this.data});

  LoginModel.fromJson(Map<String, dynamic> json) {
    status = json['status'] != null
        ? new JenisKelamin.fromJson(json['status'])
        : null;
    accessToken = json['access_token'];
    tokenType = json['token_type'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.status != null) {
      data['status'] = this.status!.toJson();
    }
    data['access_token'] = this.accessToken;
    data['token_type'] = this.tokenType;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Status {
  String? kode;
  String? keterangan;

  Status({this.kode, this.keterangan});

  Status.fromJson(Map<String, dynamic> json) {
    kode = json['kode'];
    keterangan = json['keterangan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kode'] = this.kode;
    data['keterangan'] = this.keterangan;
    return data;
  }
}

class Data {
  int? id;
  String? email;
  String? hp;
  String? firstname;
  String? lastname;
  String? grup;
  String? tglLahir;
  JenisKelamin? jenisKelamin;
  String? referralCode;
  JenisKelamin? status;
  JenisKelamin? accountStatus;
  Photo? photo;
  Null? toko;
  Null? referredBy;
  Null? statusBlokir;

  Data(
      {this.id,
      this.email,
      this.hp,
      this.firstname,
      this.lastname,
      this.grup,
      this.tglLahir,
      this.jenisKelamin,
      this.referralCode,
      this.status,
      this.accountStatus,
      this.photo,
      this.toko,
      this.referredBy,
      this.statusBlokir});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    hp = json['hp'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    grup = json['grup'];
    tglLahir = json['tgl_lahir'];
    jenisKelamin = json['jenis_kelamin'] != null
        ? new JenisKelamin.fromJson(json['jenis_kelamin'])
        : null;
    referralCode = json['referral_code'];
    status = json['status'] != null
        ? new JenisKelamin.fromJson(json['status'])
        : null;
    accountStatus = json['account_status'] != null
        ? new JenisKelamin.fromJson(json['account_status'])
        : null;
    photo = json['photo'] != null ? new Photo.fromJson(json['photo']) : null;
    toko = json['toko'];
    referredBy = json['referred_by'];
    statusBlokir = json['status_blokir'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['hp'] = this.hp;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['grup'] = this.grup;
    data['tgl_lahir'] = this.tglLahir;
    if (this.jenisKelamin != null) {
      data['jenis_kelamin'] = this.jenisKelamin!.toJson();
    }
    data['referral_code'] = this.referralCode;
    if (this.status != null) {
      data['status'] = this.status!.toJson();
    }
    if (this.accountStatus != null) {
      data['account_status'] = this.accountStatus!.toJson();
    }
    if (this.photo != null) {
      data['photo'] = this.photo!.toJson();
    }
    data['toko'] = this.toko;
    data['referred_by'] = this.referredBy;
    data['status_blokir'] = this.statusBlokir;
    return data;
  }
}

class JenisKelamin {
  dynamic kode;
  String? keterangan;

  JenisKelamin({this.kode, this.keterangan});

  JenisKelamin.fromJson(Map<String, dynamic> json) {
    kode = json['kode'];
    keterangan = json['keterangan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kode'] = this.kode;
    data['keterangan'] = this.keterangan;
    return data;
  }
}

class Photo {
  int? id;
  String? filename;
  String? caption;
  int? width;
  int? height;
  String? contentType;
  String? storage;
  String? uri;

  Photo(
      {this.id,
      this.filename,
      this.caption,
      this.width,
      this.height,
      this.contentType,
      this.storage,
      this.uri});

  Photo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    filename = json['filename'];
    caption = json['caption'];
    width = json['width'];
    height = json['height'];
    contentType = json['content_type'];
    storage = json['storage'];
    uri = json['uri'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['filename'] = this.filename;
    data['caption'] = this.caption;
    data['width'] = this.width;
    data['height'] = this.height;
    data['content_type'] = this.contentType;
    data['storage'] = this.storage;
    data['uri'] = this.uri;
    return data;
  }
}
