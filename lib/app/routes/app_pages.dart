import 'package:get/get.dart';
import 'package:koffiesoft/app/modules/auth/bindings/auth_binding.dart';
import 'package:koffiesoft/app/modules/auth/controllers/auth_controller.dart';
import 'package:koffiesoft/app/modules/auth/views/login_view.dart';
import 'package:koffiesoft/app/modules/auth/views/register_view.dart';
import 'package:koffiesoft/app/modules/auth/views/verification_view.dart';
import 'package:koffiesoft/app/modules/home/bindings/home_binding.dart';
import 'package:koffiesoft/app/modules/home/views/home_view.dart';
import 'package:koffiesoft/app/routes/app_routes.dart';

class AppPages {
  AppPages._();

  static final routes = [
    GetPage(
      name: Routes.HOME, 
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginView(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.REGISTER, 
      page: () => RegisterView(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.VERIFICATION, 
      page: () => VerificationView(),
      binding: AuthBinding(),
    ),
  ];
}
