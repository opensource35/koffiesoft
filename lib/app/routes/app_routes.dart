class Routes {
  Routes._();

  static const HOME = "/home";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
  static const VERIFICATION = "/verification";
}
