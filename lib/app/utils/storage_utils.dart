import 'package:get_storage/get_storage.dart';

class MyStorage{
  static const token = "token";

  static String? getToken(){
    return GetStorage().read(MyStorage.token);
  }
}